const mapResponse = 'map:'
const isMapResponse = (response) => {
  return response.substr(0, 5).includes(mapResponse)
}

self.document = {
  createElement(type) {
    if (type === 'canvas') {
      const canvas = new OffscreenCanvas(1, 1);
      return canvas;
    } else {
      return {
        style: {},
      };
    }
  },

  addEventListener() { },
};

importScripts('pixi.min.js');
var app
var container
self.install
self.onmessage = function (evt) {

  if (evt.data.canvas) {
    var { canvas } = evt.data;
    canvas.style = {};

    app = new PIXI.Application({
      clearBeforeRender: true,
      width: 3000,
      height: 3000,
      backgroundColor: 0x0000,
      view: canvas
    });

    container = new PIXI.Container();
    container.x = 150
    container.y = 60
    app.stage.addChild(container);
  }

  if (evt.data.message) {
    let content = evt.data.message.puzzleContent
    const style = new PIXI.TextStyle({
      fontSize: content.includes('returns valid commands') ? 22 : 16,
      fontWeight: "bold",
      fill: "#00FF41",
      padding: 50
    });
    // makes first map bigger
    if (isMapResponse(content)) {
      content = content.replace('map:\n', '')
      if (content.length < 5000) style.fontSize = 25
    }
    let puzzleContent
    // clear container before rendering new message
    for (var i = container.children.length - 1; i >= 0; i--) {
      container.removeChild(container.children[i]);
    };
    // todo - check rendering for 200k
    if (content.length < 80000) {
      puzzleContent = new PIXI.Text(content, style);
      container.addChild(puzzleContent);
    } else {
      const subContent = content.substr(0, 80000)
      puzzleContent = new PIXI.Text(subContent, style);
      container.addChild(puzzleContent);

      /**
       * TODO - to render the largest map 
          We should split it in chunks and asseble the canvas;
          something like: 

          const rowsArr = content.split('\n').filter(item => item !== '')
          groups = []
          txtGroups = []
          const slices = 5
          for (let i = 0; i < rowsArr.length; i += slices) {
            groups.push(rowsArr.slice(i, i + slices).join('\n'))
          }
          groups.forEach(row => txtGroups.push(new PIXI.Text(content.substr(0, 100000), style)))
          txtGroups.forEach((txt, index) => {
            if (index === 0) {
              container.addChild(txt);
            } else {
              prevTxtposition = txtGroups[index - 1].y
              prevTxtHeight = txtGroups[index - 1].height
              nextPosition = prevTxtposition + prevTxtHeight + 10
              txt.position.set(0, nextPosition)
              container.addChild(txt);
            }
          })

       */
    }
  }
};
