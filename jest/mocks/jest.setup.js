import '@testing-library/jest-dom';
import '@testing-library/jest-dom/extend-expect';

class Worker {
  constructor(stringUrl) {
    this.url = stringUrl;
    this.onmessage = () => { };
  }

  postMessage(msg) {
    this.onmessage(msg);
  }
}

window.Worker = Worker;
