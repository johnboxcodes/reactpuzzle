import { mapResponse, newLevelResponse, noMapResponse, verifyResponse } from "../consts/consts"
import { PuzzleGrid } from "../typings"

export const stringMapToGrid = (map: string): PuzzleGrid => {
  const rows = map.split('\n')
  const grid: PuzzleGrid = {}

  for (let rowIndex = 0; rowIndex < rows.length; rowIndex++) {
    if (rows[rowIndex]) {
      grid[`row_${rowIndex}`] = rows[rowIndex]
        .split('')
        .map((piece, colIndex) => ({ piece, row: rowIndex, col: colIndex }))
    }
  }
  return grid
}

// helpers to check message response
export const isMapResponse = (response: string): boolean => {
  return response.substr(0, 5).includes(mapResponse)
}

export const isNewLevelResponse = (response: string): boolean => {
  return response === newLevelResponse
}

export const isNoLevelResponse = (response: string): boolean => {
  return response === noMapResponse
}

export const isVerifyResponse = (response: string): boolean => {
  return response.includes(verifyResponse)
}
