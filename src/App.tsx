import React from 'react';
import { Provider } from 'react-redux';
import './App.scss';
import { store } from './app/store';
import DisplaySelector from './features/display-selector/DisplaySelector';
import PuzzleContainer from './features/puzzle/PuzzleContainer';
function App() {
  return (
    <Provider store={store}>
      <div className=".wrapper">
        <div className="header">
          <span>pipes puzzle</span>
          <DisplaySelector />
        </div>
        <PuzzleContainer />
      </div>
    </Provider>
  );
}

export default App;
