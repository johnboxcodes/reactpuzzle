export const newLevelResponse = 'new: OK'
export const mapResponse = 'map:'
export const noMapResponse = 'map: Not started'
export const verifyResponse = 'verify: '
export const pleaseLoadMap = 'No map loaded. Please choose a level on NEW menu or check help'


