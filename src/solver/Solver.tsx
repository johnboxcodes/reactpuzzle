import React, { useEffect, useMemo, useState } from 'react'
import { VariableSizeGrid as Grid } from 'react-window';

import { useAppSelector } from '../app/hooks';
import { selectGameStatus, selectMap } from '../features/puzzle/puzzleReducer';
import styles from './Solver.module.scss'

export interface ISolver {
  rotatePieceCb: (x: number, y: number) => void;
  verifySolutionCb: () => void;
  resetCb: () => void;
}

const Solver = ({ rotatePieceCb, verifySolutionCb, resetCb }: ISolver) => {
  const levelMap = useAppSelector(selectMap);
  const [grid, setGrid] = useState<string[][]>([[]])
  const [atempts, setAtempts] = useState(0)
  const gameStatus = useAppSelector(selectGameStatus);

  useEffect(() => {
    if (levelMap) {
      const grid: string[][] = []
      const gridRows = levelMap.split('\n')
      gridRows.forEach((row, index) => {
        if (row?.length > 0) {
          grid.push(row.split(''))
        }
      })
      setGrid(grid)
    }
  }, [levelMap])
  const handleRotation = (row: number, col: number) => {
    rotatePieceCb(row, col)
  }
  const reset = () => {
    setAtempts(0)
    resetCb()
  }
  const verifySolution = () => {
    if (atempts < 10) {
      setAtempts(atempts + 1)
    }
    verifySolutionCb()
  }
  // @ts-ignore 
  const Cell = ({ columnIndex, rowIndex, style }) => (
    <div style={style} className={styles.puzzleCell}
      onClick={() => handleRotation(rowIndex, columnIndex)}>
      <span>
        {grid[rowIndex][columnIndex]}
      </span>
    </div>
  );

  // @ts-ignore 
  const Row = useMemo(() => ({ index, style }) => (
    <div style={style}> {grid[index]} {index}</div>
  ), [])


  return (
    <div className={styles.wrapper}>
      {
        grid &&
        <div className={styles.gridContainer}>
          <div className={styles.status}>
            <div className={styles.verify} onClick={() => verifySolution()}>
              <span>verify solution</span>
            </div>
            {(gameStatus === "maximum atempts exceeded") &&
              <div className={styles.reset} onClick={() => reset()}>
                <span>reset</span>
              </div>
            }
            <div className={styles.gameStatus} onClick={() => verifySolution()}>
              {(gameStatus !== "maximum atempts exceeded") &&
                <span>Maximum atempts: {atempts}/10</span>
              }
              <span>{gameStatus}</span>
            </div>
          </div>
          <div className={styles.grid}>
            <Grid
              columnCount={grid[0].length}
              columnWidth={() => 12}
              rowCount={grid.length}
              rowHeight={() => 30}
              height={350}
              width={600}
            >
              {Cell}
            </Grid>
          </div>
        </div>
      }
    </div>
  )
}

export default Solver
