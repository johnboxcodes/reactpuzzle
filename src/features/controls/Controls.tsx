import React, { useState } from 'react'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import { message, selectDisplay, setLevel } from '../puzzle/puzzleReducer'

import styles from './Controls.module.scss'

const Controls = () => {
  const display = useAppSelector(selectDisplay);
  const commands = ['help', 'new', 'map'] as const
  const [showLevels, setShowLevels] = useState(false)
  const dispatch = useAppDispatch();

  const handleCommand = (command: string) => {

    if (command === 'new') {
      setShowLevels(!showLevels)
    } else {
      dispatch(message(command))
      setTimeout(() => {
        dispatch(message(null))
      }, 500);
    }
  }
  // const newLevel = (level: number) => dispatchCommand(`new ${level}`)
  const levelsClasses = `${styles.levels} ${showLevels ? styles.active : styles.inactive}`
  const levels = [1, 2, 3, 4, 5]
  const handleLevels = (level: number) => {
    dispatch(message(`new ${level}`))
    dispatch(setLevel(level))
    setShowLevels(false)
  }

  return (
    <div className={styles.wrapper}>
      {commands
        .map(command =>
          <div
            key={command}
            className={styles.commandItem}
            onClick={() => handleCommand(command)}
          >{command}</div>)
      }
      <div className={levelsClasses} onMouseLeave={() => setShowLevels(false)}>
        {levels
          .map(level =>
            <div key={level}
              className={styles.levelItem}
              onClick={() => handleLevels(level)}
            >
              <span>Level {level}</span>
            </div>)
        }
      </div>
    </div>
  )
}

export default Controls
