import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';
import { GameStatus } from '../../typings';

export interface PuzzleState {
  map: string;
  level: number;
  display: 'visualizer' | 'solver';
  message: string | null,
  gameStatus: GameStatus
}

const initialState: PuzzleState = {
  map: '',
  level: 1,
  display: 'visualizer',
  message: null,
  gameStatus: ''
};

export const puzzleReducer = createSlice({
  name: 'puzzle',
  initialState,
  reducers: {
    message: (state, action: PayloadAction<string | null>) => {
      state.message = action.payload
    },
    setMap: (state, action: PayloadAction<string>) => {
      state.map = action.payload;
    },
    setLevel: (state, action: PayloadAction<number>) => {
      state.level = action.payload;
    },
    setDisplay: (state, action: PayloadAction<'visualizer' | 'solver'>) => {
      state.display = action.payload;
    },
    setGameStatus: (state, action: PayloadAction<GameStatus>) => {
      state.gameStatus = action.payload;
    },
  },
});

export const { setMap, setLevel, setDisplay, setGameStatus, message } = puzzleReducer.actions;

export const selectMap = (state: RootState) => state.puzzle.map;
export const selectLevel = (state: RootState) => state.puzzle.level;
export const selectDisplay = (state: RootState) => state.puzzle.display;
export const selectMessage = (state: RootState) => state.puzzle.message;
export const selectGameStatus = (state: RootState) => state.puzzle.gameStatus;


export default puzzleReducer.reducer;
