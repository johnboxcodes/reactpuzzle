import React, { useEffect, useRef, useState } from 'react';
import { useAppSelector } from '../../app/hooks';
import { selectMap } from './puzzleReducer';
import styles from './Puzzle.module.scss'
import worker from '../../utils/worker';
import { newLevelResponse } from '../../consts/consts';
import { isVerifyResponse } from '../../utils/utils';
export interface IPuzzle {
  display: 'visualizer' | 'solver',
  lastMessage: string
}
const Puzzle = ({ display, lastMessage }: IPuzzle) => {
  const [notSupported, setNotSupported] = useState(true)

  const canvasRef = useRef<HTMLCanvasElement>(null);
  const canvas = canvasRef.current;

  const fallBackMessage = {
    value: ''
  }

  useEffect(() => {
    if (canvas) {

      // @ts-ignore
      if (canvas.transferControlToOffscreen) {
        setNotSupported(false)
        fallBackMessage.value = `This app purpose is demonstrational. The visualizer uses a experimental
        technology that your browser doesn't support.
        Please use the last version Google Chrome or Edge Browser.`
        // @ts-ignore
        const offscreen = canvas!.transferControlToOffscreen();
        worker.postMessage({ canvas: offscreen }, [offscreen]);
        worker.postMessage({ message: { value: 'update', puzzleContent: `Load` } });
      }
    }
  }, [canvas])
  useEffect(() => {
    if (lastMessage) {
      if (lastMessage !== newLevelResponse && !isVerifyResponse(lastMessage)) {
        worker.postMessage({ message: { value: 'update', puzzleContent: lastMessage } });
      }
    }
  }, [lastMessage])

  return (
    <div className={styles.wrapper}>
      <canvas width={0} height={0} ref={canvasRef}></canvas>
      {
        notSupported &&
        <div className={styles.fallbackMessage}>
          <span>
            {fallBackMessage.value}
          </span>
        </div>
      }
    </div>
  );
}

export default Puzzle