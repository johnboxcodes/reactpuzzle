import React, { useEffect } from "react";
import useWebSocket, { ReadyState } from 'react-use-websocket';
import { useAppSelector, useAppDispatch } from '../../app/hooks';

import { selectDisplay, selectMessage, setGameStatus, setMap } from './puzzleReducer';
import Puzzle from "./Puzzle";
import Controls from "../controls/Controls";
import styles from './PuzzleContainer.module.scss'
import { mapResponse, newLevelResponse, pleaseLoadMap } from "../../consts/consts";
import Solver from "../../solver/Solver";
import { DisplayOptions, GameStatus } from "../../typings";
import { isMapResponse, isNewLevelResponse, isNoLevelResponse, isVerifyResponse } from "../../utils/utils";
import worker from "../../utils/worker";


const PuzzleContainer = () => {
  const display = useAppSelector(selectDisplay);
  const showMessage = useAppSelector(selectMessage);
  const URL = "wss://hometask.eg1236.com/game-pipes/";
  const dispatch = useAppDispatch();

  const {
    sendMessage,
    lastMessage,
    readyState,
  } = useWebSocket(URL, {
    onMessage: message => handleMessage(message.data)
  });

  const connectionStatus = {
    [ReadyState.CONNECTING]: 'Connecting',
    [ReadyState.OPEN]: 'Open',
    [ReadyState.CLOSING]: 'Closing',
    [ReadyState.CLOSED]: 'Closed',
    [ReadyState.UNINSTANTIATED]: 'Uninstantiated',
  }[readyState];

  const handleMessage = (message: string) => {
    if (isNewLevelResponse(message)) {
      sendMessage('map')
    }
    if (isMapResponse(message)) {
      const levelMap = message.split('\n')
      levelMap.shift()
      const levelMapStr = levelMap.join('\n')
      dispatch(setMap(levelMapStr))
      dispatch(setGameStatus(''))
    }
    if (isNoLevelResponse(message)) {
      setTimeout(() => {
        worker.postMessage({ message: { value: 'update', puzzleContent: pleaseLoadMap } });
      }, 1500);
    }
    if (isVerifyResponse(message)) {
      const response = message
        .split(' ')[1]
        .replace('.', '')
        .toLowerCase() as GameStatus
      if (response.toUpperCase() === "ONLY") {
        dispatch(setGameStatus('maximum atempts exceeded'))
      } else {
        dispatch(setGameStatus(response))
      }
    }
  }

  const handleDisplayClass = (displayComponent: DisplayOptions) => {
    return (displayComponent === display) ? `${styles.display}`
      : `${styles.display} ${styles.hide}`
  }


  useEffect(() => {
    if (connectionStatus === 'Open') {
      //
      sendMessage('new 1')
      // loads the help after connection is good,
      setTimeout(() => {
        sendMessage('help')
      }, 1000);
    }
    return () => {
    }
  }, [connectionStatus])

  useEffect(() => {
    if (showMessage) {
      sendMessage(showMessage)
    }
  }, [showMessage, sendMessage])

  const rotatePiece = (row: number, col: number) => {
    // TODO implement multiple pieces rotation
    sendMessage(`rotate ${col} ${row}`)
    setTimeout(() => {
      sendMessage('map')
    }, 50);
  }
  const verifyStatus = () => {
    sendMessage('verify')
  }

  const reset = () => {
    sendMessage('new 1')
  }

  if (connectionStatus === 'Open') {
    return (
      <div className={styles.wrapper}>
        <div className={styles.controls}>
          <Controls />
        </div>
        < div className={handleDisplayClass('visualizer')}>
          <Puzzle display={display} lastMessage={lastMessage?.data} />
        </div>
        < div className={handleDisplayClass('solver')}>
          <Solver rotatePieceCb={rotatePiece}
            verifySolutionCb={verifyStatus}
            resetCb={reset} />
        </div>
      </div >
    )
  }
  return (
    <div className={styles.loadingWrapper}>
      <div className={styles.loading}>
        <span>{connectionStatus}</span>
      </div>
    </div>
  )
}

export default PuzzleContainer