import React, { useState } from 'react'
import { useAppDispatch } from '../../app/hooks'
import { DisplayOptions } from '../../typings'
import { setDisplay } from '../puzzle/puzzleReducer'
import styles from './DisplaySelector.module.scss'
const DisplaySelector = () => {
  const [active, setActive] = useState<DisplayOptions>('visualizer')

  const dispatch = useAppDispatch();

  const handleActive = (selected: DisplayOptions) => {
    return active === selected ? styles.active : ''
  }
  const handleSelection = (selection: DisplayOptions) => {
    setActive(selection)
    dispatch(setDisplay(selection))
  }
  return (
    <div className={styles.wrapper}>
      <div className={styles.box}>
        <div className={handleActive('visualizer')}
          onClick={() => handleSelection('visualizer')}>visualizer</div>
        <div className={handleActive('solver')}
          onClick={() => handleSelection('solver')}>solver</div>
      </div>
    </div>
  )
}

export default DisplaySelector
