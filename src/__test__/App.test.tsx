import React from 'react'

import { render, screen, waitFor } from '@testing-library/react';
import App from "../App"

test('renders app', async () => {
  render(<App />);
  const element = await waitFor(() => screen.getByText(/puzzle/i))
  expect(element).toBeInTheDocument()
});
