export type DisplayOptions = 'visualizer' | 'solver'
export type GameStatus = 'correct' | 'incorrect' | '' | 'maximum atempts exceeded'
export type Column = {
  piece: string;
  row: number;
  col: number;
}
export interface PuzzleGrid {
  [key: string]: Column[]
}