## Pipes Puzzle

This is a version of the popular Pipes Puzzle, also known as
FreeNet or NetWalk
## Demo online 🕹️

Check it online 👉 [here](https://puzzle.johnboxcodes.com/)

## Install
```yarn```

## Run ⚙️
```yarn serve```

### Dev server
```yarn dev```

### Build
```yarn build```

## About development

* The main concern was to present a solid app
that could handle the challenge and show some ideas on
how to deal with a huge amount of data.

* For the visualizer component, that renders the maps and help
on a canvas, I chosed to use the experimental offscreenCanvas
that would allow to process the data in a service worker and,
by doing this, avoid blocking the UI (the main thread would
hang trying to handle the larger maps).

* As a helper to work with canvas I used PixiJS, but it doesn't
support offscreenCanvas, so It had to have a little patch (
  declaring the Document on the service worker.
)

* For the solver, I used React-window to partially render the list
of puzzle-pieces 


#### State Management

Started using Context API but switched to Redux with ReduxToolKit.

#### Ideas: develop a version with webassembly for better performance

#### Solutions

Couldn't solve any! Please play it online and help
me find those!

#### Known limitation

* Rendering texts above 100k chars breaks the format
on PixiJS. Would have to implement a treatment for that
(like layering the results)[there's a commented code on the service worker with
a possible solution that splits the response in chunks] or do a custom solution
to calculate and print the text to canvas properly.

* There was a conscient choice of using experimental offscreenCanvas
so it will run only on latest versions of Google Chrome and Microsoft
Edge browsers.

Although it breaks the _format_, even with 1million chars
it would not hang the main thread of block the UI.





